'use strict'

var gulp = require('gulp'),
    sass = require('gulp-sass'),
    browserSync = require('browser-sync'),
    del = require('del'),
    imagemin = require('gulp-imagemin'),
    uglify = require('gulp-uglify'),
    usemin = require('gulp-usemin'),
    rev = require('gulp-rev'),
    cleancss = require('gulp-clean-css'),
    flatmap = require('gulp-flatmap'),
    htmlmin = require('gulp-htmlmin')
    ;


function genSass () {
    return gulp.src('./css/*.scss')
        .pipe(sass().on('error', sass.logError))
        .pipe(gulp.dest('./css'));
};

function watchGenSass(){
    gulp.watch('./css/*.scss', gulp.series('sass'));
};

function browserSyncing(){
    var files = ['./*.html', './css/*.css', './images/*.{png,jpg,gif,jpeg}', './js/*.js'];
    browserSync.init(files, {
        server: {
            baseDir: "./"
        }
    });
}

function cleanDir(){
    return del(['distri']);
}

function copyMyFonts(){
    return gulp.src('./node_modules/open-iconic/font/fonts/*.{ttf,woff,eof,svg,eot,otf}*')
        .pipe(gulp.dest('./distri/fonts'));
}

function minimizeImages(){
    return gulp.src('./images/*.{png,jpg,jpeg,gif}')
        .pipe(imagemin({optimizationLevel: 3, progressive: true, interlaced: true}))
        .pipe(gulp.dest('distri/images'));
}

function useMinize(){
    return gulp.src('*.html')
        .pipe(flatmap(function(stream, file){
            return stream
            .pipe(usemin({
                css: [rev()],
                html: [function() {return htmlmin({collapseWhitespace: true})}],
                js: [uglify(), rev()],
                inlinejs: [uglify()],
                inlinecss: [cleancss(), 'concat']
            }));
        }))
        .pipe(gulp.dest('distri/'));
}

//Tasks Individuals
gulp.task('sass', genSass);
gulp.task('sass:watch', watchGenSass);
gulp.task('browser-sync', browserSyncing);
gulp.task('clean', cleanDir);
gulp.task('copyfonts', copyMyFonts);
gulp.task('imagemin', minimizeImages);
gulp.task('usemin',useMinize);

//Define Parallels
gulp.task('default', gulp.parallel('browser-sync', 'sass:watch'));
gulp.task('build', gulp.series('clean','copyfonts','imagemin','usemin'));
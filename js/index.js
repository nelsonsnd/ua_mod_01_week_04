$(document).ready(function () {
    $('[data-toggle="tooltip"]').tooltip();
});
$('.carousel').carousel({
    interval: 2000
});

$('#incursionContact').on('show.bs.modal', function(e){
    console.log('el modal se esta mostrando');

    $('.btn-ex').removeClass('btn-primary');
    $('.btn-ex').addClass('btn-outline-success');
    $('.btn-ex').prop('disabled',true);
  });
  $('#incursionContact').on('shown.bs.modal', function(e){
    console.log('el modal se mostró');
  });
  $('#incursionContact').on('hide.bs.modal', function(e){
    console.log('se esta escondiendo');
    $('.btn-ex').removeClass('btn-outline-success');
    $('.btn-ex').addClass('btn-primary');
    $('.btn-ex').prop('disabled',false);
  });
  $('#incursionContact').on('hidden.bs.modal', function(e){
    console.log('se escondio');
  });
